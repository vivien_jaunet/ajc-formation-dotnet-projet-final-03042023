﻿using menu.api;
using projet_twitch.api;
using projet_twitch.api.Adapters;
using projet_twitch.api.Exceptions;
using System;
using System.Collections.Generic;
using twitch_random.infrastructure;

// Delegates affichage, lecture et clear
AfficherConsole afficher = Console.WriteLine;
LecteurConsole input = Console.ReadLine;
Clear clear = Console.Clear;

//Instanciation du menu
Menu menu = new(mess =>
{
    Console.ForegroundColor = ConsoleColor.DarkGreen;
    afficher(mess);
    Console.ForegroundColor = ConsoleColor.White;
}, Console.ReadLine);

//Ajout des éléments du menu
menu.Ajouter(new MenuItem(1, "Ajouter un membre de la communauté", 1));
menu.Ajouter(new MenuItem(2, "Ajouter un cadeau", 2));
menu.Ajouter(new MenuItem(3, "Afficher la liste des membres de la communuaté", 3));
menu.Ajouter(new MenuItem(4, "Afficher la liste des cadeaux", 4));
menu.Ajouter(new MenuItem(5, "Attribuer des cadeaux aléatoirement", 5));
menu.Ajouter(new MenuItem(6, "Sauvegarder la liste de la communauté", 6));
menu.Ajouter(new MenuItem(7, "Sauvegarder la liste des cadeaux", 7));
var monbooleen = false;

// préparation des sauvegardes 
string cheminSauvegarde = Path.Combine(Environment.CurrentDirectory, $"save-commu-{DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")}.json");
var communauteList = new CommunauteList(new JsonTwitchSaver(cheminSauvegarde));

string cheminSauvegarde2 = Path.Combine(Environment.CurrentDirectory, $"save-cadeau-{DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")}.json");
var cadeauList = new CadeauList(new JsonTwitchSaver(cheminSauvegarde2));

//Liste de la communauté
List<Communaute> listCommunaute = new()
{
    new(1,"Samantha","Sasuke","samantha@fff.f" ),
    new(2,"pssssss","nsssss","aaaa@sdf.fr" ),
    new(3,"pbbbbb","nqqqqq","pppooiuuy@sdfsf.fr" ),
    new(4,"pazazazaz","nrrrrrrr","ooio@sdfsf.fr" ),
    new(5,"pbbreree","nqddsqdqsd","jkjkjk@sdfsf.fr" ),
    new(6,"pbbtytytb","nqqqqqsvbvbvqq","aaagfg@sdfsf.fr" ),
    new(7,"pbbfsdsqfgb","nqqddddddq","zqzrtttrzz@sdfsf.fr" ),
    new(8,"pbbqsdqsdb","nqsdqsd","zsssseez@sdfsf.fr" ),
};
communauteList.AddListCommunaute(listCommunaute);

//Liste des cadeaux
List<Cadeau> listCadeau = new()
{
    new() {Id = 1,   NomCadeau= "Cadeau zezejrz"},
    new() {Id = 2,   NomCadeau= "Cadeau sdfjsdf"},
    new() {Id = 3,   NomCadeau= " "},
    new() {Id = 4,   NomCadeau= "Cadeau saaasdf"},
    new() {Id = 5,   NomCadeau= " "},
    new() {Id = 6,   NomCadeau= " "},
    new() {Id = 7,   NomCadeau= "Cadeau sefdddf"},
    new() {Id = 8,   NomCadeau= " "},
    new() {Id = 9,   NomCadeau= " "},
    new() {Id = 10,  NomCadeau= "Cadeau zerzrsz"},
    new() {Id = 11,  NomCadeau= "Cadeau zrzbrzz"},
    new() {Id = 12,  NomCadeau= " "},
};
cadeauList.AddListCadeau(listCadeau);

//Menu principal
AfficherMenu();

void AfficherMenu()
{
    // Affichage en dictionnaire du menu
    Dictionary<int, Action> menuActions = new()
    {
        {1, () => AjouterFollower() },
        {2, () => AjouterCadeau() },
        {3, () => AfficherListCommunaute() },
        {4, () => AfficherListCadeau() },
        {5, () => AttribuerDesCadeaux() },
        {6, () => SauvegardeCommunaute() },
        {7, () => SauvegardeCadeau() }
    };
    do
    {
        menu.Afficher();
        if (int.TryParse(Console.ReadLine(), out int input))
        {
            try
            {
                menuActions[input]();
            }
            catch (Exception)
            {
                //throw new ChoixMenuPAsAttenduException(afficher);
            }
        }
    } while (!monbooleen);
}

void AjouterFollower()
{
    clear();
    //formulaire follower
    afficher("Ajouter une personne dans la communaute :");
    afficher("Id : (plus de 8) ");
    string ajoutPersoId = input();
    int intId = int.Parse(ajoutPersoId);
    afficher("Prenom :");
    string ajoutPersoPrenom = input();
    afficher("Nom :");
    string ajoutPersoNom = input();
    afficher("Email :");
    string ajoutPersoEmail = input();
    //ajout follower
    Communaute ajoutPersonneCommunaute = new Communaute(intId, ajoutPersoPrenom, ajoutPersoNom, ajoutPersoEmail);
    listCommunaute.Add(ajoutPersonneCommunaute);
    afficher("Ajout d'un membre de la communauté réussi !");
    AfficherMenu();
}

void AjouterCadeau()
{
    clear();
    //formulaire cadeau
    afficher("Ajouter un cadeau :");
    afficher("Id : (plus que 13)");
    string ajoutKdoId = input();
    int intIdKdo = int.Parse(ajoutKdoId);
    afficher("Nom du cadeau :");
    string ajoutNomKdo = input();
    //ajout cadeau
    Cadeau ajoutCadeau = new Cadeau(intIdKdo, ajoutNomKdo);
    listCadeau.Add(ajoutCadeau);
    afficher("Ajout de cadeau réussi !");
    AfficherMenu();
}

void AfficherListCommunaute()
{
    // Affichage de la communauté
    clear();
    foreach (Communaute commu in listCommunaute)
    {
        afficher(commu.Prenom.ToString());
    }
    AfficherMenu();
}

void AfficherListCadeau()
{
    // Affichage des cadeaux
    clear();
    foreach (Cadeau cadeaux in listCadeau)
    {
        afficher(cadeaux.NomCadeau.ToString());
    }
    AfficherMenu();
}

void AttribuerDesCadeaux()
{
    // Attribution des cadeaux aux abonnés
    clear();
    for (int i = 0; i < listCadeau.Count; i++)
    {
        var rand = new Random();
        int gagnant1 = rand.Next(1, 8);
        int gagnant2 = rand.Next(1, 8);
        int gagnant3 = rand.Next(1, 8);
        do
        {
            gagnant1 = rand.Next(1, 8);
            gagnant2 = rand.Next(1, 8);
            gagnant3 = rand.Next(1, 8);
        } while (gagnant1 == gagnant2 || gagnant1 == gagnant3 || gagnant2 == gagnant3);

        var query = from persos in listCommunaute
                    where persos.Id == gagnant1 && persos.Id == i || persos.Id == gagnant2 && persos.Id == i || persos.Id == gagnant3 && persos.Id == i
                    select persos;
        var query2 = from cadeaux in listCadeau
                     where cadeaux.Id == gagnant1 && cadeaux.Id == i || cadeaux.Id == gagnant2 && cadeaux.Id == i || cadeaux.Id == gagnant3 && cadeaux.Id == i
                     select cadeaux;
        foreach (var perso in query)
        {
            foreach (var kdo in query2)
            {
                afficher($"{perso.Prenom} a gagné le cadeau suivant : {kdo.NomCadeau}");
            }
        }
    }
    AfficherMenu();
}

void SauvegardeCommunaute()
{
    // Sauvegarde de la communauté
    clear();
    try
    {
        communauteList.SaveListCommunaute();
        cadeauList.SaveListCadeau();
    }
    catch (NotImplementedException ex)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Oops erreur !", ex.Message);
        Console.ForegroundColor = ConsoleColor.White;
    }
    catch (Exception ex)
    {

    }
    afficher("Sauvegarde de liste de la communauté effectué");
    AfficherMenu();
}

void SauvegardeCadeau()
{
    // Sauvegarde des cadeaux
    clear();
    try
    {
        cadeauList.SaveListCadeau();
    }
    catch (NotImplementedException ex)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Oops erreur !", ex.Message);
        Console.ForegroundColor = ConsoleColor.White;
    }
    catch (Exception ex)
    {

    }
    afficher("Sauvegardede de la liste des cadeaux effectué");
    AfficherMenu();
}
