﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    /// <summary>
    /// Un item du menu
    /// </summary>
    public class MenuItem
    {
        #region Properties
        public int Id { get; set; }

        public string Libelle { get; set; } = "";

        public int OrdreAffichage { get; set; }
        #endregion

        #region Constructor
        public MenuItem(int id, string libelle, int ordreAffichage)
        {
            Id = id;
            Libelle = libelle;
            OrdreAffichage = ordreAffichage;
        }
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return $"{this.Id} : {this.Libelle}";
        }
        #endregion
    }
}
