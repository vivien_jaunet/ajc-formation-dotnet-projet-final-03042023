﻿using projet_twitch.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_twitch.api
{
    /// <summary>
    /// Permet de sauvegarder une liste de communaute
    /// </summary>
    public class CommunauteList
    {
        #region Properties
        private List<Communaute> List { get; set; }
        #endregion

        #region Fields
        private readonly ITwitchSave saver;
        #endregion

        #region Constructor
        /// <summary>
        /// Passe en param l'interface pour sauvegarder la communauté
        /// </summary>
        /// <param name="saver"></param>
        public CommunauteList(ITwitchSave saver) => this.saver = saver;
        #endregion

        #region Public Methods
        /// <summary>
        /// Ajout d'une liste de communauté
        /// </summary>
        /// <param name="list"></param>
        public void AddListCommunaute(List<Communaute> list)
        {
            this.List = list;
        }
        /// <summary>
        /// Sauvegarde de la liste de la Communauté
        /// </summary>
        public void SaveListCommunaute()
        {
            this.saver.SaveCommunaute(this.List);
        }

        #endregion
    }
}
