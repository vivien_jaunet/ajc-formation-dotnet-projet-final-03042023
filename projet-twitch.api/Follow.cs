﻿using System.ComponentModel.DataAnnotations;

namespace projet_twitch.api
{
    /// <summary>
    /// Personne de la communauté qui est abonné au streamer
    /// </summary>
    public class Follow
    {
        #region Properties
        public int Id { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Le Surnom est obligatoire.")]
        public string Surnom { get; set; } = null!;

        public string ImageProfil { get; set; }

        public bool Connection { get; set; }

        public int PresenceTotal { get; set; }
        #endregion
    }
}
