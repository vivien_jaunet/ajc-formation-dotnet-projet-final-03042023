﻿using projet_twitch.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_twitch.api
{
    /// <summary>
    /// Communauté du streamer
    /// </summary>
    public class Communaute/* : CommunauteList*/
    {
        #region Properties
        public int Id { get; set; }
        public string Prenom { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Instancie un follower de la communauté
        /// </summary>
        /// <param name="id"></param>
        /// <param name="prenom"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        public Communaute(int id, string prenom, string name, string email)
        {
            Id = id;
            Prenom = prenom;
            Name = name;
            Email = email;
        }
        #endregion

    }

}