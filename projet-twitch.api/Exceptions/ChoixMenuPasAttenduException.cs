﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_twitch.api.Exceptions
{
    /// <summary>
    /// Classe pour gérer l'erreur d'un mauvais choix dans le menu
    /// </summary>
    public class ChoixMenuPAsAttenduException : Exception
    {
        public string Infomsg { get; private set; } = "Ce numéro n'est pas lié au menu, veuillez ressayer.";
        public ChoixMenuPAsAttenduException(AfficherConsole afficher)
        {
            afficher(Infomsg);
        }
    }
}

