﻿using System.ComponentModel.DataAnnotations;
using System.Reflection.PortableExecutable;

namespace projet_twitch.api
{
    /// <summary>
    /// Le streamer sur twitch
    /// </summary>
    public class Streamer
    {
        #region Properties
        public int Id { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "La date est obligatoire.")]
        public DateTime DateS { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Le titre est obligatoire.")]
        public string Title { get; set; } = null!;

        [StringLength(100)]
        [Required(ErrorMessage = "Le statut est obligatoire.")]
        public string Statut { get; set; } = null!;
        #endregion
    }
}
