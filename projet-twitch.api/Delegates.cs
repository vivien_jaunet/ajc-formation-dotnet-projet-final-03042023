﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_twitch.api
{
    /// <summary>
    /// Contrat de méthode pour afficher un message
    /// </summary>
    /// <returns></returns>
    public delegate void AfficherConsole(string msg);

    /// <summary>
    /// Contrat de méthode pour afficher un message
    /// </summary>
    /// <returns></returns>
    public delegate string? LecteurConsole();

    /// <summary>
    /// Contrat de méthode pour afficher un Console Clear
    /// </summary>
    /// <returns></returns>
    public delegate void Clear();
}
