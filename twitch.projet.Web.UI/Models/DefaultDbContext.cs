﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using projet_twitch.api;

namespace twitch.projet.Web.UI.Models
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }

        /// <summary>
        /// C'est une LISTE ! et elle sera connectée à une table en bdd
        /// </summary>
        public DbSet<Follow> Follows { get; set; }
    }
}